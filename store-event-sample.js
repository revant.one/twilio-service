const EventStore = require('geteventstore-promise');
const testStream = 'nest-starter';
const client = new EventStore.TCPClient({
  hostname: 'localhost',
  port: 1113,
  credentials: {
    username: 'admin',
    password: 'changeit',
  },
});

client
  .writeEvent(testStream, 'UserLogInHOTPGeneratedEvent', {
    something: 'special',
  })
  .then(() => client.getEvents(testStream, undefined, 1, 'backward'))
  .then(events => console.info(events[0]))
  .catch(error => console.error({ error: error.message }))
  .finally(() =>
    client
      .close()
      .then(closed => console.log('Disconnected'))
      .catch(error => console.error({ error: error.message })),
  );
