import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({}).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('No Operation (Useless Test)', done => done());

  afterAll(async () => {
    await app.close();
  });
});
