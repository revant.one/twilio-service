import { Injectable, Inject } from '@nestjs/common';
import { Twilio } from 'twilio';
import { TWILIO_CONNECTION } from '../../twilio.provider';
import { ConfigService, TWILIO_NUMBER } from '../../../common/config.service';

@Injectable()
export class MessengerService {
  constructor(
    @Inject(TWILIO_CONNECTION)
    private readonly client: Twilio,
    private readonly config: ConfigService,
  ) {}

  async sendMessage(body: string, to: string) {
    const from = this.config.get(TWILIO_NUMBER);
    return await this.client.messages.create({ body, to, from });
  }

  async sendPhoneVerificationMessage(payload) {
    const { data } = payload;
    if (data && data.hotp) {
      const { phoneOTP } = data;
      if (phoneOTP && phoneOTP.metaData && phoneOTP.metaData.phone) {
        let message = `Phone verification OTP ${data.hotp}.`;
        message += `Code expires on ${phoneOTP.expiry}. Do not share with anyone.`;
        try {
          await this.sendMessage(message, phoneOTP.metaData.phone);
        } catch (error) {}
      }
    }
  }

  async sendLoginOTPtoPhone(payload) {
    const { data } = payload;
    if (data && data.hotp) {
      const { user } = data;
      if (user && user.phone) {
        const message = `Login OTP ${data.hotp}. Do not share with anyone.`;
        try {
          await this.sendMessage(message, user.phone);
        } catch (error) {}
      }
    }
  }
}
