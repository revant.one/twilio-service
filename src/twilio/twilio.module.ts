import { Module } from '@nestjs/common';
import { TwilioProvider } from './twilio.provider';
import { TwilioAggregates } from './aggregates';
import { TwilioController } from './controllers';

@Module({
  providers: [...TwilioProvider, ...TwilioAggregates],
  exports: [...TwilioProvider, ...TwilioAggregates],
  controllers: [...TwilioController],
})
export class TwilioModule {}
