import * as Joi from 'joi';
import * as dotenv from 'dotenv';
import { Injectable } from '@nestjs/common';

export interface EnvConfig {
  [prop: string]: string;
}

export const NODE_ENV = 'NODE_ENV';
export const DB_NAME = 'DB_NAME';
export const DB_HOST = 'DB_HOST';
export const DB_USER = 'DB_USER';
export const DB_PASSWORD = 'DB_PASSWORD';
export const ES_HOST = 'ES_HOST';
export const ES_USER = 'ES_USER';
export const ES_PASSWORD = 'ES_PASSWORD';
export const ES_STREAM = 'ES_STREAM';
export const TWILIO_SID = 'TWILIO_SID';
export const TWILIO_AUTH_TOKEN = 'TWILIO_AUTH_TOKEN';
export const TWILIO_NUMBER = 'TWILIO_NUMBER';

@Injectable()
export class ConfigService {
  private readonly envConfig: EnvConfig;

  constructor() {
    const config = dotenv.config().parsed;
    this.envConfig = this.validateInput(config);
  }

  /**
   * Ensures all needed variables are set, and returns the validated JavaScript object
   * including the applied default values.
   */
  private validateInput(envConfig: EnvConfig): EnvConfig {
    const envVarsSchema: Joi.ObjectSchema = Joi.object({
      NODE_ENV: Joi.string()
        .valid(['development', 'production', 'test', 'provision', 'staging'])
        .default('development'),
      ES_HOST: Joi.string().required(),
      ES_USER: Joi.string().required(),
      ES_PASSWORD: Joi.string().required(),
      ES_STREAM: Joi.string().required(),
      TWILIO_SID: Joi.string().required(),
      TWILIO_AUTH_TOKEN: Joi.string().required(),
      TWILIO_NUMBER: Joi.string().required(),
    });

    const { error, value: validatedEnvConfig } = Joi.validate(
      envConfig,
      envVarsSchema,
    );
    if (error) {
      throw new Error(`Config validation error: ${error.message}`);
    }
    return validatedEnvConfig;
  }

  get(key: string): string {
    return this.envConfig[key];
  }

  getEventStoreConfig() {
    const hostname = this.get(ES_HOST);
    const username = this.get(ES_USER);
    const password = this.get(ES_PASSWORD);
    const stream = this.get(ES_STREAM);
    return { hostname, username, password, stream };
  }
}
