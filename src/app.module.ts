import { Module } from '@nestjs/common';
import { CommonModule } from './common/common.module';
import { SystemSettingsModule } from './system-settings/system-settings.module';
import { EventStoreModule } from './event-store/event-store.module';
import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './system-settings/aggregates/terminus-options/terminus-options.service';
import { TwilioModule } from './twilio/twilio.module';

@Module({
  imports: [
    TerminusModule.forRootAsync({ useClass: TerminusOptionsService }),
    CommonModule,
    SystemSettingsModule,
    EventStoreModule,
    TwilioModule,
  ],
})
export class AppModule {}
