import { Module, Global } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';

@Global()
@Module({ imports: [CqrsModule] })
export class SystemSettingsModule {}
